#!/usr/bin/env python

#
# Standard geni-lib/portal libraries
#
import geni.portal as portal
import geni.rspec.pg as rspec
import geni.rspec.emulab as elab
import geni.rspec.igext as IG
import geni.urn as URN


tourDescription = """
Use this profile to instantiate an experiment using Open Air Interface
to realize an end-to-end SDR-based mobile network. This profile includes
the following resources:

  * SDR eNodeB (Intel NUC + USRP B210) running OAI on Ubuntu 16 ('enb1')
  * One or more UEs of the requested type (Nexus 5 or NUC + B210 SDR)
  * All-in-one EPC node (HSS, MME, SPGW) running OAI on Ubuntu 16 ('epc')
  * A node providing out-of-band ADB access to the UE ('adb-tgt')

PhantomNet startup scripts automatically configure OAI for the
specific allocated resources (aside from OAI UE nodes, which require
manual service startup).

Most of the detailed information available for the default Powder profile also
applies to this profile.  More info here:

  * [Getting Started](https://gitlab.flux.utah.edu/powder-profiles/OAI-Real-Hardware/blob/master/README.md)

""";

tourInstructions = """
After booting is complete, log onto either the `enb1` or `epc` nodes. From there, you will be able to start all OAI services across the network by running:

    sudo /local/repository/bin/start_oai.pl

Above command will stop any currently running OAI services, start all services (both epc and enodeb) again, and then interactively show a tail of the logs of the mme and enodeb services. Once you see the logs, you can exit at any time with Ctrl-C, but the services stay running in the background and save logs to `/var/log/oai/*` on the `enb1` and `epc` nodes.

You will have to log in to the UE node and manually start up OAI services.  Note that the OAI UE code contained on this node (under `/opt/OAI/openairinterface5g`) is preliminary and may not work.

While OAI is still a system in development and may be unstable, you can usually recover from any issue by running `start_oai.pl` to restart all the services.

  * [Full Documentation](https://gitlab.flux.utah.edu/powder-profiles/OAI-Real-Hardware/blob/master/README.md)

""";


#
# PhantomNet extensions.
#
import geni.rspec.emulab.pnext as PN

#
# Globals
#
class GLOBALS(object):
    OAI_DS = "urn:publicid:IDN+emulab.net:phantomnet+ltdataset+oai-develop"
    OAI_UE_IMG  = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:OAI-Real-Hardware.enb1")
    OAI_EPC_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:UBUNTU16-64-OAIEPC")
    OAI_ENB_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:OAI-Real-Hardware.enb1")
    OAI_CONF_SCRIPT = "/usr/bin/sudo /local/repository/bin/config_oai.pl"
    ADB_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:UBUNTU14-64-PNTOOLS")
    NUC_HWTYPE = "nuc5300"
    NEXUS5_HWTYPE = "nexus5"
    NEXUS5_IMG = URN.Image(PN.PNDEFS.PNET_AM, "PhantomNet:ANDROID444-STD")

def connectOAI_DS(node):
    # Create remote read-write clone dataset object bound to OAI dataset
    bs = request.RemoteBlockstore("ds-%s" % node.name, "/opt/oai")
    bs.dataset = GLOBALS.OAI_DS
    bs.rwclone = True

    # Create link from node to OAI dataset rw clone
    node_if = node.addInterface("dsif_%s" % node.name)
    bslink = request.Link("dslink_%s" % node.name)
    bslink.addInterface(node_if)
    bslink.addInterface(bs.interface)
    bslink.vlan_tagging = True
    bslink.best_effort = True

#
# This geni-lib script is designed to run in the PhantomNet Portal.
#
pc = portal.Context()

#
# Profile parameters.
#
pc.defineParameter("UECNT", "UE Count",
                   portal.ParameterType.INTEGER,1,
                   longDescription="The number of UE devices/nodes you wish to have connected to the eNodeB in this experiment.  No more than four are available simultaneously to the same eNodeB.")
pc.defineParameter("UETYPE", "UE Type",
                   portal.ParameterType.STRING,"nexus5",[("nexus5","Nexus 5 Phone"),("nuc5300","NUC with B210 (OAI)")],
                   longDescription="Choose between either Nexus 5 off-the-shelf handsets, or Intel NUCs connected to Ettus B210 SDRs for the UE devices.")


pc.defineParameter("TYPE", "Experiment type",
                   portal.ParameterType.STRING,"atten",[("atten","Controlled RF connection"),("ota","Over the air")],
                   longDescription="*Controlled RF connection*: Real RF devices will be connected via transmission lines with variable attenuator control. *Over the air*: Real RF devices with real antennas and transmissions propagated through free space will be selected.")

params = pc.bindParameters()

if params.UECNT < 1 or params.UECNT > 4:
    pc.reportError(portal.ParameterError(
        "Number of UEs must be between 1 and 4.",
        ["UECNT"]))

#
# Give the library a chance to return nice JSON-formatted exception(s) and/or
# warnings; this might sys.exit().
#
pc.verifyParameters()

#
# Create our in-memory model of the RSpec -- the resources we're going
# to request in our experiment, and their configuration.
#
request = pc.makeRequestRSpec()

# Add a node to act as the ADB target host, if using Nexus 5 UEs
if params.UETYPE == "nexus5":
    adb_t = request.RawPC("adb-tgt")
    adb_t.disk_image = GLOBALS.ADB_IMG

# Add a NUC+SDR eNB node.
enb = request.RawPC("enb1")
enb.hardware_type = GLOBALS.NUC_HWTYPE
enb.disk_image = GLOBALS.OAI_ENB_IMG
enb.Desire( "rf-radiated" if params.TYPE == "ota" else "rf-controlled", 1 )
connectOAI_DS(enb)
enb.addService(rspec.Execute(shell="sh", command=GLOBALS.OAI_CONF_SCRIPT + " -r ENB"))

# Add the number of UEs requested of the requested type.  Each will have
# an RF link defined between it and the 'enb' node.
for i in range(1, params.UECNT + 1):
    ue = None
    if params.UETYPE == "nexus5":
        ue = request.UE("nxue%d" % i)
        ue.hardware_type = GLOBALS.NEXUS5_HWTYPE
        ue.disk_image = GLOBALS.NEXUS5_IMG
        ue.adb_target = "adb-tgt"
    else:
        ue = request.RawPC("nucue%d" % i)
        ue.hardware_type = GLOBALS.NUC_HWTYPE
        ue.disk_image = GLOBALS.OAI_UE_IMG
        connectOAI_DS(ue)
    ue.Desire("rf-radiated" if params.TYPE == "ota" else "rf-controlled", 1)
    rflink = request.RFLink("rflink%d" % i)
    rflink.addNode(ue)
    rflink.addNode(enb)

# Add OAI EPC (HSS, MME, SPGW) node.
epc = request.RawPC("epc")
epc.disk_image = GLOBALS.OAI_EPC_IMG
epc.addService(rspec.Execute(shell="sh", command=GLOBALS.OAI_CONF_SCRIPT + " -r EPC"))
connectOAI_DS(epc)

# Add a link connecting the NUC eNB and the OAI EPC node.
epclink = request.Link("s1-lan", members=[enb,epc])
epclink.link_multiplexing = True
epclink.vlan_tagging = True
epclink.best_effort = True

tour = IG.Tour()
tour.Description(IG.Tour.MARKDOWN, tourDescription)
tour.Instructions(IG.Tour.MARKDOWN, tourInstructions)
request.addTour(tour)

#
# Print and go!
#
pc.printRequestRSpec(request)
